package com.testng.demo;

/**
 * Hello name!
 *
 */
public class SimpleStringClass
{
    public static void hello( String name )
    {
        System.out.println( "Hello " + name + "!" );
    }
}
