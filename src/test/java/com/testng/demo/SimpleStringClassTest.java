package com.testng.demo;


import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


/**
 * Unit test for simple SimpleStringClass.
 */
public class SimpleStringClassTest{

    @Parameters({ "username", "password", "expectedMessage"})
    @Test
    public void validateSimpleString(@Optional("tomsmith") String username,
                                         @Optional("SuperSecretPassword!") String password,
                                         @Optional("You logged into a secure area!") String expectedMessage)  {

        System.out.println("Assert Ray = Ray");
        Assert.assertEquals("Ray", "Ray");

    }



}
